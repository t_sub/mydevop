public with sharing class DiffClass1 {
    public DiffClass1() {
        System.debug('hello everyone');
        System.debug('inside the constructor');
    }

    public static void testDiffClassMethod(){
        System.debug('inside the testDiffClassMethod');
    }

    public static void testDiffClassMethod1(){
        System.debug('inside the testDiffClassMethod1');
    }

    public static void testDiffClassMethod2(){
        System.debug('inside the testDiffClassMethod2');
    }

    public static void testDiffClassMethod3(){
        System.debug('inside the testDiffClassMethod3');
    }

    public static void testDiffClassMethod4(){
        System.debug('inside the testDiffClassMethod4');
    }

    public static void testDiffClassMethod5(){
        System.debug('inside the testDiffClassMethod5');
    }

    public static void testDiffClassMethod6(){
        System.debug('inside the testDiffClassMethod6');
    }
}